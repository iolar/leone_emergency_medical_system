
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 8
Total Travel Time in minutes: 40


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 12
Total Travel Time in units: 30
Total Travel Time in minutes: 150
ALERT! Total Travel Time exceeds the deadline of 30 minutes!


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 29
Total Travel Time in minutes: 145
ALERT! Total Travel Time exceeds the deadline of 25 minutes!


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 0
Total Travel Time in minutes: 0


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 26
Total Travel Time in minutes: 130
ALERT! Total Travel Time exceeds the deadline of 10 minutes!

 OPTIMAL BASE STATIONS
12.Yengema	| Tot Average Daily Calls: 23	Percentage: 37.096775%
14.Kamera	| Tot Average Daily Calls: 7	Percentage: 11.290323%
18.Makeni	| Tot Average Daily Calls: 32	Percentage: 51.612904%

 AMBULANCES DISTRIBUTION
12.Yengema	| Tot Average Daily Calls: 23	Percentage: 23%	Assigned Ambulances: 14.83871 -> 15
14.Kamera	| Tot Average Daily Calls: 7	Percentage: 7%	Assigned Ambulances: 4.516129 -> 5
18.Makeni	| Tot Average Daily Calls: 32	Percentage: 32%	Assigned Ambulances: 20.64516 -> 21
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 3
Gathering Centers: 8
Number of Base Stations used: 3 out of 3
Alerts: 3 over 8 gathering centers
Ambulances: 40
Total Average Daily Calls: 62
Ambulances over Daily Calls: Lack of 22
