
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 8
Total Travel Time in minutes: 40


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 12
Total Travel Time in units: 33
Total Travel Time in minutes: 165
ALERT! Total Travel Time exceeds the deadline of 45 minutes!


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 32
Total Travel Time in minutes: 160
ALERT! Total Travel Time exceeds the deadline of 40 minutes!


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 0
Total Travel Time in minutes: 0


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 30
Total Travel Time in minutes: 150
ALERT! Total Travel Time exceeds the deadline of 30 minutes!

 OPTIMAL BASE STATIONS
12.	Yengema	| Tot Average Daily Calls: 76	Percentage: 38.0%
14.	Kamera	| Tot Average Daily Calls: 18	Percentage: 9.0%
18.	Makeni	| Tot Average Daily Calls: 106	Percentage: 53.0%

 AMBULANCES DISTRIBUTION
12.	Yengema	| Tot Average Daily Calls: 76	Percentage: 38.0%	Assigned Ambulances: 114.0 -> 114
14.	Kamera	| Tot Average Daily Calls: 18	Percentage: 9.0%	Assigned Ambulances: 27.0 -> 27
18.	Makeni	| Tot Average Daily Calls: 106	Percentage: 53.0%	Assigned Ambulances: 159.0 -> 159
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 3
Gathering Centers: 8
Number of Base Stations used: 3 out of 3
Alerts: 3 over 8 gathering centers
Ambulances: 300
Total Average Daily Calls: 200
Ambulances over Daily Calls: Excess of 100
