
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 5
Total Travel Time in units: 6
Total Travel Time in minutes: 30


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 7
Total Travel Time in units: 13
Total Travel Time in minutes: 65


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 15
Total Travel Time in units: 15
Total Travel Time in minutes: 75


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 0
Total Travel Time in minutes: 0


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 13 Karina
Nearest BaseStation: 21
Total Travel Time in units: 19
Total Travel Time in minutes: 95

 OPTIMAL BASE STATIONS
5.Simria	| Tot Average Daily Calls: 20	Percentage: 10.0%
7.Kulanko	| Tot Average Daily Calls: 20	Percentage: 10.0%
12.Yengema	| Tot Average Daily Calls: 56	Percentage: 28.0%
15.Sinegal	| Tot Average Daily Calls: 18	Percentage: 9.0%
18.Makeni	| Tot Average Daily Calls: 33	Percentage: 16.5%
21.Suniya	| Tot Average Daily Calls: 53	Percentage: 26.5%

 AMBULANCES DISTRIBUTION
5.Simria	| Tot Average Daily Calls: 20	Percentage: 20%	Assigned Ambulances: 30.0 -> 30
7.Kulanko	| Tot Average Daily Calls: 20	Percentage: 20%	Assigned Ambulances: 30.0 -> 30
12.Yengema	| Tot Average Daily Calls: 56	Percentage: 56%	Assigned Ambulances: 84.0 -> 84
15.Sinegal	| Tot Average Daily Calls: 18	Percentage: 18%	Assigned Ambulances: 27.0 -> 27
18.Makeni	| Tot Average Daily Calls: 33	Percentage: 33%	Assigned Ambulances: 49.5 -> 50
21.Suniya	| Tot Average Daily Calls: 53	Percentage: 53%	Assigned Ambulances: 79.5 -> 80
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 10
Gathering Centers: 8
Number of Base Stations used: 6 out of 10
Alerts: 0 over 8 gathering centers
Ambulances: 300
Total Average Daily Calls: 200
Ambulances over Daily Calls: Excess of 100
