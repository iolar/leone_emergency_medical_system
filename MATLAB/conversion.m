%%
PHU_year_calls = zeros(1412,1);

%% count PHU yearly calls

for i=1:1412
    for j=1:32796
        if(strcmpi(PHU_calls_str(j,1),PHU(i,1))) % case insensitive
            PHU_year_calls(i,1) = PHU_year_calls(i,1) + 1;
            disp(i + " - " + PHU(i,1) + ": " + PHU_year_calls(i,1));
        end
    end
    disp(i + " - " + PHU(i,1) + " calls counted");
end

%% round 2 and delete comma to coordinates, keep 2 decimals
N_AS    = 82;
N_H     = 41;
N_PHU   = 1412;
xy_round_AS     = zeros(82,2);
xy_round_H      = zeros(41,2);
xy_round_PHU    = zeros(1412,2);

% AS
for i=1:N_AS
    for j=1:2
        xy_round_AS(i,j) = round(xy_AS(i,j)*100);
        if j == 1 && xy_round_AS(i,j) > 0
            xy_round_AS(i,j) = - xy_round_AS(i,j); % because wrong Sierra Leone longitude
        end
    end
    display(i + " - " + AS(i,1) + " " + xy_round_AS(i,1) + ", " + xy_round_AS(i,2));
end
% H
for i=1:N_H
    for j=1:2
        xy_round_H(i,j) = round(xy_H(i,j)*100);
        if j == 1 && xy_round_H(i,j) > 0
            xy_round_H(i,j) = - xy_round_H(i,j); % because wrong Sierra Leone longitude
        end
    end
    display(i + " - " + H(i,1) + " " + xy_round_H(i,1) + ", " + xy_round_H(i,2));
end
% PHU
c = 0;
for i=1:N_PHU
    for j=1:2
        xy_round_PHU(i,j) = round(xy_PHU(i,j)*100);
        if j == 1 && xy_round_PHU(i,j) > 0
            xy_round_PHU(i,j) = - xy_round_PHU(i,j); % because wrong Sierra Leone longitude
        end
    end
    display(i + " - " + PHU(i,1) + " " + xy_round_PHU(i,1) + ", " + xy_round_PHU(i,2));
end

%% Costs from day fraction to minutes
N_AS    = 82;
N_H     = 41;
N_PHU   = 1412;

MATRIX_AS_PHU_min   = NaN(N_PHU,N_AS);    % PAY ATTENTION TO DIMENSIONS!
MATRIX_PHU_H_min    = NaN(N_PHU,N_H);     % PAY ATTENTION TO DIMENSIONS!
MATRIX_H_AS_min     = NaN(N_AS,N_H);      % PAY ATTENTION TO DIMENSIONS!

% AS - PHU
for i=1:N_PHU
    for j=1:N_AS
        if(not(isnan(MATRIXASPHUcomplete(i,j))))
            MATRIX_AS_PHU_min(i,j) = round(MATRIXASPHUcompletev2(i,j) *24*60);
        end
    end
end

% PHU - H
for i=1:N_PHU
    for j=1:N_H
        if(not(isnan(MATRIXPHUHcomplete(i,j))))
            MATRIX_PHU_H_min(i,j) = round(MATRIXPHUHcompletev2(i,j) *24*60);
        end
    end
end

% H - AS
for i=1:N_AS
    for j=1:N_H
        if(not(isnan(MATRIXHAScomplete(i,j))))
            MATRIX_H_AS_min(i,j) = round(MATRIXHAScompletev2(i,j) *24*60);
        end
    end
end


%% Construct input matrix without names and without adjacencies
N = 1535;

N_AS    = 82;
N_H     = 41;
N_PHU   = 1412;

INPUT_NO_NAME_NO_ADJ = zeros(N,5);
key = 0;

% H
for i=1:N_H
    INPUT_NO_NAME_NO_ADJ(i,1)   = key;              % KEY
    INPUT_NO_NAME_NO_ADJ(i,2)   = xy_round_H(i,1);  % X (LONGITUDINE)
    INPUT_NO_NAME_NO_ADJ(i,3)   = xy_round_H(i,2);  % Y (LATITUDINE)
    INPUT_NO_NAME_NO_ADJ(i,4)   = 0;                % TYPE
    INPUT_NO_NAME_NO_ADJ(i,5)   = 0;                % CALLS
    key = key + 1;
end

index = N_H;

% AS
for i=(index+1):(index+N_AS)
    INPUT_NO_NAME_NO_ADJ(i,1)   = key;                    % KEY
    INPUT_NO_NAME_NO_ADJ(i,2)   = xy_round_AS(i-index,1); % X (LONGITUDINE)
    INPUT_NO_NAME_NO_ADJ(i,3)   = xy_round_AS(i-index,2); % Y (LATITUDINE)
    INPUT_NO_NAME_NO_ADJ(i,4)   = 1;                      % TYPE
    INPUT_NO_NAME_NO_ADJ(i,5)   = 0;                      % CALLS
    key = key + 1;
end

index = N_H + N_AS;

% PHU
for i=(index+1):(index+N_PHU)
    INPUT_NO_NAME_NO_ADJ(i,1)   = key;                    % KEY
    INPUT_NO_NAME_NO_ADJ(i,2)   = xy_round_PHU(i-index,1);% X (LONGITUDINE)
    INPUT_NO_NAME_NO_ADJ(i,3)   = xy_round_PHU(i-index,2);% Y (LATITUDINE)
    INPUT_NO_NAME_NO_ADJ(i,4)   = 2;                      % TYPE
    INPUT_NO_NAME_NO_ADJ(i,5)   = PHU_year_calls(i-index);% CALLS
    key = key + 1;
end

%% Construct input matrix names
INPUT_NAMES = [H; AS; PHU];


%% Construct input matrix with adjacencies
N = 1535;

N_AS    = 82;
N_H     = 41;
N_PHU   = 1412;

ADJ_index = zeros(N,1);
for i = 1:N
    ADJ_index(i,1) = 1;
end

INPUT_ADJ = zeros(N,N);
INPUT_ADJ = string(INPUT_ADJ); % to string

PHU_index = N_H + N_AS;
AS_index = N_H;
H_index = 0;

% AS - PHU
for i=1:N_PHU
    for j=1:N_AS
        if not(isnan(MATRIX_AS_PHU_min(i,j)))
            % add AS adj to PHU node
            AS_key = AS_index + j - 1;                                  % adj key
            adj = AS_key + " " + MATRIX_AS_PHU_min(i,j);                % string
            
            INPUT_ADJ(i+PHU_index, ADJ_index(i+PHU_index, 1)) = adj;    % save
            
            ADJ_index(i+PHU_index, 1) = ADJ_index(i+PHU_index, 1) + 1;  % increment
            
            % add PHU adj to AS node
            PHU_key = PHU_index + i - 1;                                % adj key
            adj = PHU_key + " " + MATRIX_AS_PHU_min(i,j);               % string
            
            INPUT_ADJ(j+AS_index, ADJ_index(j+AS_index, 1)) = adj;      % save
            
            ADJ_index(j+AS_index, 1) = ADJ_index(j+AS_index, 1) + 1;    % increment
        end
    end
end

% PHU - H
for i=1:N_PHU
    for j=1:N_H
        if not(isnan(MATRIX_PHU_H_min(i,j)))
            % add H adj to PHU node
            H_key = H_index + j - 1;                                  % adj key
            adj = H_key + " " + MATRIX_PHU_H_min(i,j);                % string
            
            INPUT_ADJ(i+PHU_index, ADJ_index(i+PHU_index, 1)) = adj;    % save
            
            ADJ_index(i+PHU_index, 1) = ADJ_index(i+PHU_index, 1) + 1;  % increment
            
            % add PHU adj to H node
            PHU_key = PHU_index + i - 1;                                % adj key
            adj = PHU_key + " " + MATRIX_PHU_H_min(i,j);               % string
            
            INPUT_ADJ(j+H_index, ADJ_index(j+H_index, 1)) = adj;      % save
            
            ADJ_index(j+H_index, 1) = ADJ_index(j+H_index, 1) + 1;    % increment
        end
    end
end

% H - AS
for i=1:N_AS
    for j=1:N_H
        if not(isnan(MATRIX_H_AS_min(i,j)))
            % add H adj to AS node
            H_key = H_index + j - 1;                                  % adj key
            adj = H_key + " " + MATRIX_H_AS_min(i,j);                 % string
            
            INPUT_ADJ(i+AS_index, ADJ_index(i+AS_index, 1)) = adj;    % save
            
            ADJ_index(i+AS_index, 1) = ADJ_index(i+AS_index, 1) + 1;  % increment
            
            % add AS adj to H node
            AS_key = AS_index + i - 1;                                % adj key
            adj = AS_key + " " + MATRIX_H_AS_min(i,j);               % string
            
            INPUT_ADJ(j+H_index, ADJ_index(j+H_index, 1)) = adj;      % save
            
            ADJ_index(j+H_index, 1) = ADJ_index(j+H_index, 1) + 1;    % increment
        end
    end
end


% clean zeros with blank strings
for i = 1:N
    for j = 1:N
        if strcmp(INPUT_ADJ(i,j),"0")
            INPUT_ADJ(i,j) = "";
        end
    end
end


% find maximum index in ADJ_index to know from which
% column delete blank columns
max = ADJ_index(1,1);
index = 1;
for i = 1:N
    if ADJ_index(i,1) > max
        max = ADJ_index(i,1);
        index = i;
    end
end

disp(index + " " + max);

% delete blank columns
for i = N:-1:max
    INPUT_ADJ(:,i) = [];
end


%% Construct table with matrix data
INPUT_T = table(INPUT_NO_NAME_NO_ADJ, INPUT_NAMES, INPUT_ADJ);

%% Write table
writetable(INPUT_T, "INPUT_FILE.xlsx");



