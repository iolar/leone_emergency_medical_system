# Leone_emergency_medical_system ![](https://icons.iconarchive.com/icons/matthew-kelleigh/mac-town-vol2/32/Ambulance-1-icon.png)

![](https://icons.iconarchive.com/icons/icons8/windows-8/32/Programming-Administrative-Tools-icon.png) Dynamic programming techniques for optimal positioning of emergency medical services in rural countries ![](https://icons.iconarchive.com/icons/fatcow/farm-fresh/32/world-icon.png)

<br><br>
Performance Simulation Input Data:
<br>
- <b><i>0_n23_a40_Scattered_bs_c_00.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>scattered</b> graph with <b>3</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a40_Scattered_bs_c_01.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>scattered</b> graph with <b>3</b> base stations (8 gathering centers) and <b>200</b> calls
- <b><i>0_n23_a40_Scattered_bs_c_10.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>scattered</b> graph with <b>10</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a40_Scattered_bs_c_11.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>scattered</b> graph with <b>10</b> base stations (8 gathering centers) and <b>200</b> calls

- <b><i>0_n23_a40_Dense_bs_c_00.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>dense</b> graph with <b>3</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a40_Dense_bs_c_01.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>dense</b> graph with <b>3</b> base stations (8 gathering centers) and <b>200</b> calls
- <b><i>0_n23_a40_Dense_bs_c_10.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>dense</b> graph with <b>10</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a40_Dense_bs_c_11.txt</b></i> :   <b>23</b> nodes, <b>40</b> ambulances on <b>dense</b> graph with <b>10</b> base stations (8 gathering centers) and <b>200</b> calls

- <b><i>0_n23_a300_Scattered_bs_c_00.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>scattered</b> graph with <b>3</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a300_Scattered_bs_c_01.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>scattered</b> graph with <b>3</b> base stations (8 gathering centers) and <b>200</b> calls
- <b><i>0_n23_a300_Scattered_bs_c_10.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>scattered</b> graph with <b>10</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a300_Scattered_bs_c_11.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>scattered</b> graph with <b>10</b> base stations (8 gathering centers) and <b>200</b> calls

- <b><i>0_n23_a300_Dense_bs_c_00.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>dense</b> graph with <b>3</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a300_Dense_bs_c_01.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>dense</b> graph with <b>3</b> base stations (8 gathering centers) and <b>200</b> calls
- <b><i>0_n23_a300_Dense_bs_c_10.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>dense</b> graph with <b>10</b> base stations (8 gathering centers) and <b>62</b> calls
- <b><i>0_n23_a300_Dense_bs_c_11.txt</b></i> :   <b>23</b> nodes, <b>300</b> ambulances on <b>dense</b> graph with <b>10</b> base stations (8 gathering centers) and <b>200</b> calls

- <b><i>0_n40_a300_Scattered_bs_c_00.txt</b></i> :   <b>40</b> nodes, <b>300</b> ambulances on <b>scattered</b> graph with <b>5</b> base stations (14 gathering centers) and <b>115</b> calls
- <b><i>0_n40_a300_Dense_bs_c_00.txt</b></i> :   <b>40</b> nodes, <b>300</b> ambulances on <b>dense</b> graph with <b>5</b> base stations (14 gathering centers) and <b>115</b> calls

<br><br>
<b>Algorithm Execution Time File Data</b>:
<br>
Composed by three numbers<br>
- first number is the summation of all the times through the executions;<br>
- second number is the number of the executions;<br>
- third number is the average (summation/executions' number)




<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
Project Avatar Designed By 30000011179 From <a href="https://lovepik.com/image-401405888/lion-avatar-cartoon-flat-vector.html">LovePik.com</a>




