package main_package;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;

import data_structures.adj_node;
import data_structures.dist_node;

// Graphs

public class SL_graphs {

	final static int N_NODES = SL_data.getNodes();
	final static String graph_name = SL_data.getGraphName();
	private static Graph[] graphs = new Graph[N_NODES]; // graph for every node

	// constructor
	public SL_graphs() {
	}

	// GET
	public static Graph getGraph(int key) {
		return graphs[key];
	}

	// Generate Graph for node
	public static void generateGraph(int key) {
		graphs[key] = nodeGraph(graph_name + "_nodo_" + key); // generate graph for node
		graphs[key].setAttribute("ui.stylesheet", // set graph style-sheet
				"graph { fill-color: #c9c9c9; }" + "node {	size: 20px, 20px; " + "text-size: 10;" + "text-style: bold;"
						+ "text-color: red;" + "text-background-mode: rounded-box;" + "text-background-color: white;"
						+ "text-padding: 3px, 2px;" + "text-offset: 0px, 3px;" + "shape: circle; "
						+ "fill-color: white; " + "stroke-mode: plain; " + "stroke-color: black;"
						+ "shadow-mode: gradient-vertical;" + "shadow-width: 4px;" + "shadow-color: black, #c9c9c9;"
						+ "shadow-offset: 0px;" + "}" + "edge {text-size: 10;" + "size: 2;" + "text-color: white;"
						+ "text-style: bold;" + "text-background-mode: rounded-box;" + "text-padding: 2px, 1px;"
						+ "text-background-color: black;}");

		graphs[key].setAttribute("ui.title", graph_name + ": Dijkstra on node " + key); // set graph window title
		graphs[key].nodes().forEach(n -> { // set nodes' specific colors
			switch (SL_data.getNode_type(Integer.valueOf(n.getId()))) {
			case 0:
				n.setAttribute("ui.style", "fill-color: #8ec3f5; text-color: #8ec3f5;");
				break;// H node
			case 1:
				if (SL_data.getOptBS(Integer.valueOf(n.getId())) == null) {
					n.setAttribute("ui.style", "fill-color: #84a174; text-color: #84a174;");
					break;// inactive BS node
				} else
					n.setAttribute("ui.style", "fill-color: #88e354; text-color: #88e354;");
				break;// active BS node
			case 2:
				n.setAttribute("ui.style", "fill-color: #ff4f6f; text-color: #ff4f6f;");
				break;// G node
			case 3:
				n.setAttribute("ui.style", "size: 10px, 10px;" + "fill-color: white; text-color: black;");
				break;// generic node
			default:
				break;
			}
		});
	}

	// Construct Node Graph
	public static Graph nodeGraph(String name) {
		Graph g = new SingleGraph(name);

		// Links' matrix, triangular:
		// helps constructing the graph during adjacency list scrolling
		boolean[][] link_matrix = new boolean[N_NODES][N_NODES];
		for (int i = 0; i < N_NODES; i++) {
			for (int j = 0; j < N_NODES; j++) {
				if (j > i)
					link_matrix[i][j] = true;
				else
					link_matrix[i][j] = false;
			}
		}

		for (int i = 0; i < N_NODES; i++) {
			g.addNode(String.valueOf(i)).setAttribute("xyz", SL_data.getNodeX(i), SL_data.getNodeY(i), 0);
		}

		for (int i = 0; i < N_NODES; i++) { // Set Edges into Edges' Matrix
			adj_node p = SL_data.getAdjList(i).head;
			while (!(p == null)) {
				if (link_matrix[i][p.key] == true) {
					String node1key = String.valueOf(i);
					String node2key = String.valueOf(p.key);
					g.addEdge(node1key + "-" + node2key, node1key, node2key).setAttribute("len", p.cost);
					SL_data.setEdge(i, p.key, node1key + "-" + node2key);
				} else
					SL_data.setEdge(i, p.key, null);
				p = p.next;
			}
		}

		String[] type = new String[4]; // type labels
		type[0] = "[H]";
		type[1] = "[BS]";
		type[2] = "[G]";
		type[3] = "";

		// set nodes' labels
		g.nodes().forEach(
				n -> n.setAttribute("ui.label", n.getId() + "." + type[SL_data.getNode_type(Integer.valueOf(n.getId()))]
						+ " " + SL_data.getNode_name(Integer.valueOf(n.getId()))));
		// set edges' labels
		g.edges().forEach(e -> e.setAttribute("label", "" + (int) e.getNumber("len")));
		return g;
	}

	// Highlight node's Dijkstra Path on graph
	public static void highlightShortestPath(int g) {
		for (int i = 0; i < N_NODES; i++) {
			dist_node dn = SL_data.getDistMatrix(g, i);
			if (dn.prev != -1) {
				if (SL_data.getEdge(i, dn.prev) != null) {
					graphs[g].getEdge(SL_data.getEdge(i, dn.prev)).setAttribute("ui.style", "fill-color: red;");
				} else {
					graphs[g].getEdge(SL_data.getEdge(dn.prev, i)).setAttribute("ui.style", "fill-color: red;");
				}
			}
		}
	}
}