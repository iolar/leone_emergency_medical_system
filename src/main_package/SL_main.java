
////////////////////////////////////////
//	Leone Emergency Medical System
/////		Giulia Guarino
///////			2022
////////////////////////////////////////

package main_package;

import java.util.Scanner;

import org.graphstream.ui.swing_viewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

import data_analysis.DataAnalysis;

public class SL_main {

	final static String graph_name = SL_data.getGraphName();
	final static int ADJ_ROW_START = SL_data.getAdjRowStart();
	final static String path = SL_data.getPath();

	final static String filename = SL_data.getDataFilename();
	static int N_NODES;

	static int kc; // key counter
	static int node_counter; // node counter

	static boolean only_gatherings = true; // apply Dijkstra only on Gatherings nodes
	static boolean only_optimal = true; // apply Dijkstra until, for Gathering node, Optimal Hospital and Optimal Base
										// Station is found (used with "only_gatherings")
	static boolean optimized = only_gatherings && only_optimal; // to optimize execution time and get only optimal
																// configuration (nearest H and nearest BS) without BS
																// ranking
	static boolean with_graphs = true; // generate graphs
	static boolean timeslots = false; // consider time slots
	static boolean prints = false; // use System.out.print or not

	static boolean only_some_nodes = true; // if is wanted to generate graphs only for some particular nodes
	static int[] graph_node_keys = { 1029, 1031, 1036, 1220, 1243 }; // insert nodes key of which visualize
																				// graph

	// TIMES
	static long startTime;
	static long endTime;
	static long algStartTime;
	static long algEndTime;

	static long totExecTime;
	static long totAlgoTime;

	public static void main(String args[]) throws InterruptedException {

		startTime = System.currentTimeMillis(); // acquire total start execution time (ms)

		node_counter = acquireFileData.getFileData();
		N_NODES = SL_data.getNodes();

		if (node_counter != N_NODES) {
			System.out.println("Inserted nodes' number is different respect to counted rows in file!");
			return;
		}

		if (prints) {
			SL_data.printDistMatrix();
			SL_data.printAdjLists();
		}

		// DIJKSTRA for every node to fill Distances Matrix
		System.out.println("_________________________________________________");
		System.setProperty("org.graphstream.ui", "swing"); // system properties for graphs
		algStartTime = System.currentTimeMillis(); // acquire algorithm start execution time (ms)
		for (kc = 0; kc < N_NODES; kc++) {
			if (only_gatherings) {
				if (SL_data.getNode_type(kc) == 2) {
					Process_Dijkstra_PriorityQueue dijkstra_pq = new Process_Dijkstra_PriorityQueue(kc, optimized,
							prints);
					dijkstra_pq.elab(); // apply Dijkstra on node
				}
			} else {
				Process_Dijkstra_PriorityQueue dijkstra_pq = new Process_Dijkstra_PriorityQueue(kc, optimized, prints);
				dijkstra_pq.elab(); // apply Dijkstra on node
			}
		}
		// GRAPHS
		if (with_graphs) {
			if (only_some_nodes) { // graphs only for selected nodes
				for (int key : graph_node_keys) {
					if (only_gatherings) {
						if (SL_data.getNode_type(key) == 2)
							graph(key);
					} else {
						graph(key);
					}
				}
			} else { // graphs for all nodes
				for (kc = 0; kc < N_NODES; kc++) {
					if (only_gatherings) {
						if (SL_data.getNode_type(kc) == 2)
							graph(kc);
					} else {
						graph(kc);
					}
				}
			}
			SL_data.printEdges();
		}
		// Ambulances Optimal Distribution
		SL_data.distributeAmbulances();

		algEndTime = System.currentTimeMillis(); // acquire algorithm end execution time (ms)
		totAlgoTime = algEndTime - algStartTime;

		DataAnalysis DA = new DataAnalysis(SL_data.getDataAnalysisFolder());

		analyzeData(DA); // Data Analysis

		if (timeslots) {
			// Second Time Slot Calls
			newTimeSlot(2);
			analyzeTimeSlotData(DA, 2);
			// Third Time Slot Calls
			newTimeSlot(3);
			analyzeTimeSlotData(DA, 3);
		}

		System.gc();

		// SIMULATION
		System.out.println("\n Start Real-time Simulation? (y/n)");
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		in.close();
		if (s.equals("y")) {
			dynamicSimulation DS = new dynamicSimulation(only_gatherings);
			DS.simulate();
		}
		//

		System.gc();

		endTime = System.currentTimeMillis(); // acquire total end execution time (ms)

		totExecTime = endTime - startTime;
		System.out.println("Total Execution Time: " + totExecTime + " ms");
		System.out.println("Total Algorithm Time: " + totAlgoTime + " ms");
		System.out.println("\n");
	}

	// Graph
	private static void graph(int kc) {
		SL_graphs.generateGraph(kc); // generate graph for node
		SL_graphs.highlightShortestPath(kc); // highlight Dijkstra path on node's graph
		Viewer viewer = SL_graphs.getGraph(kc).display(false);
		ViewPanel view = (ViewPanel) viewer.getDefaultView(); // ViewPanel is the view for gs-ui-swing
		view.getCamera().setViewCenter(SL_data.getNodeX(kc), SL_data.getNodeY(kc), 0);
		view.getCamera().setViewPercent(0.05);
	}

	// Data Analysis
	private static void analyzeData(DataAnalysis DA) {
		System.out.println("\t___DATA ANALYSIS___\n");

		if (!with_graphs) { // write on file algorithm execution time
			if (only_gatherings)
				DA.writeFileExecTime(filename + "_gath.txt", totAlgoTime);
			else
				DA.writeFileExecTime(filename + "_all.txt", totAlgoTime);
		}

		String filename_ext = filename + ".txt";
		DA.cleanDataFile(filename_ext); // clean file from previous execution data

		SL_data.printDistMatrix();
		SL_data.printGatheringsOpt(true, DA, filename_ext);
		SL_data.printBaseStationsOpt(true, DA, filename_ext);

		System.out.println("TOTAL AMBULANCES NUMBER: " + SL_data.getAmbulances());
		System.out.println("TOTAL AVERAGE CALLS: " + SL_data.getTotalDailyCalls());

		SL_data.printAmbulancesDistribution(true, DA, filename_ext);

		DA.appendSimulationData(filename_ext, "----------------------------------------");
		DA.appendSimulationData(filename_ext, "Nodes: " + SL_data.getNodes());
		DA.appendSimulationData(filename_ext, "Hospitals: " + SL_data.getH());
		DA.appendSimulationData(filename_ext, "Base Stations: " + SL_data.getBS());
		DA.appendSimulationData(filename_ext, "Gathering Centers: " + SL_data.getG());
		DA.appendSimulationData(filename_ext,
				"Number of Base Stations used: " + SL_data.getOptBSCount() + " out of " + SL_data.getBS());
		DA.appendSimulationData(filename_ext,
				"Alerts: " + SL_data.getAlerts() + " over " + SL_data.getG() + " gathering centers");
		DA.appendSimulationData(filename_ext, "Ambulances: " + SL_data.getAmbulances());
		DA.appendSimulationData(filename_ext, "Total Average Calls: " + SL_data.getTotalDailyCalls());
		int coverage = SL_data.getAmbulances() - SL_data.getTotalDailyCalls();
		if (coverage > 0) {
			DA.appendSimulationData(filename_ext, "Ambulances over Calls: Excess of " + coverage);
		} else if (coverage < 0) {
			DA.appendSimulationData(filename_ext, "Ambulances over Calls: Lack of " + (-1) * coverage);
		} else {
			DA.appendSimulationData(filename_ext, "Ambulances over Calls: Exact Number");
		}
	}

	// Load New Time Slot
	private static void newTimeSlot(int timeslot) {
		// reset time slot calls
		SL_data.setTimeSlotCalls(0);

		// reset Optimal BaseStations' variables for new time slot
		for (int i = 0; i < N_NODES; i++) {
			if (SL_data.getOptBS(i) != null) {
				SL_data.getOptBS(i).newTimeSlot();
			}
		}

		// load Gathering Centers with new average calls
		int node_counter = acquireFileData.getFileDataTimeSlot(timeslot);
		if (node_counter != N_NODES) {
			System.out.println("Inserted nodes' number is different respect to counted rows in file!");
			return;
		}

		// fill Optimal BaseStations' variables for new time slot
		for (int i = 0; i < N_NODES; i++) {
			if (SL_data.getOpt(i) != null) { // for every gathering center
				int average_ts_calls = SL_data.getOpt(i).getAverageTimeSlotCalls(); // get average time slot calls of
																					// Gathering Center
				int BS = SL_data.getOpt(i).nearestBS(); // get Optimal BaseStation for that Gathering Center
				SL_data.getOptBS(BS).fillTimeSlot(average_ts_calls, SL_data.getTotalDailyCalls()); // update Optimal BS
																									// calls
			}
		}

		// Ambulances Optimal Distribution
		SL_data.distributeAmbulances();
	}

	// Time Slot Data Analysis
	private static void analyzeTimeSlotData(DataAnalysis DA, int timeslot) {
		System.out.println("\n\t___TIME SLOT " + timeslot + " DATA ANALYSIS___\n");

		String filename_ext = filename + ".txt";

		DA.appendSimulationData(filename_ext, "----------------------------------------");
		DA.appendSimulationData(filename_ext, "\t___TIME SLOT " + timeslot + " DATA ANALYSIS___\n");
		SL_data.printBaseStationsOpt(true, DA, filename_ext);

		System.out.println("TOTAL AMBULANCES NUMBER: " + SL_data.getAmbulances());
		System.out.println("TOTAL AVERAGE TIME SLOT " + timeslot + " CALLS: " + SL_data.getTimeSlotCalls());

		SL_data.printAmbulancesDistribution(true, DA, filename_ext);

		DA.appendSimulationData(filename_ext, "Ambulances: " + SL_data.getAmbulances());
		DA.appendSimulationData(filename_ext,
				"Total Average Time Slot " + timeslot + " Calls: " + SL_data.getTimeSlotCalls());
		int coverage = SL_data.getAmbulances() - SL_data.getTimeSlotCalls();
		if (coverage > 0) {
			DA.appendSimulationData(filename_ext,
					"Ambulances over Time Slot " + timeslot + " Calls: Excess of " + coverage);
		} else if (coverage < 0) {
			DA.appendSimulationData(filename_ext,
					"Ambulances over Time Slot " + timeslot + " Calls: Lack of " + (-1) * coverage);
		} else {
			DA.appendSimulationData(filename_ext, "Ambulances over Time Slot " + timeslot + " Calls: Exact Number");
		}
	}
}