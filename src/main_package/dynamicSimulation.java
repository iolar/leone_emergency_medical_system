package main_package;

import dynamic_simulation.*;

import java.util.concurrent.ConcurrentLinkedQueue;

import data_structures.dist_node;

public class dynamicSimulation {

	private final static int N_NODES = SL_data.getNodes(); // Total Number of Nodes
	private final static int N_OPT_BS = SL_data.getOptBSCount(); // Number Of Optimal Base Stations

	private static Integer[] producedCalls; // Gathering centers' produced calls array (the index is the key of the
											// gathering node)
	private static Integer[] optimalBS; // Optimal BS Array for Gathering Centers to know where to send calls
	private static Integer[] travelTime; // Travel Times for every Gathering Center to be assisted from its own optimal
											// BaseStation to Optimal Hospital
	private static gathering_time[] simulationTravelWaitingTime; // Travel Time + Waiting Time in simulation for every
																	// Gathering Center to be assisted from its own
																	// optimal BaseStation to Optimal Hospital
	private static Integer[] ambulancesReturnTimes; // Return Times for every gathering center from the nearest hospital
													// to the optimal base station

	private Call_Producer Call_Producer; // Call Producer
	private Dispatcher Dispatcher; // Call Dispatcher for Base Stations' queues
	private Base_Station[] BS; // Base Stations

	private ConcurrentLinkedQueue<Integer> cq; // Entering Calls Queue
	private ConcurrentLinkedQueue<Integer>[] bs_q; // Dispatched Calls Queues (dispatched for Base Stations)

	private static Thread[] BS_Threads = new Thread[N_OPT_BS]; // Thread for every Base Station
	private static Thread Producer_Thread;
	private static Thread Dispatcher_Thread;

	private Window w;

	// constructor
	public dynamicSimulation(boolean only_gatherings) {

		w = new Window(N_OPT_BS);

		producedCalls = new Integer[N_NODES];
		optimalBS = new Integer[N_NODES];
		travelTime = new Integer[N_NODES];
		simulationTravelWaitingTime = new gathering_time[N_NODES];
		ambulancesReturnTimes = new Integer[N_NODES];

		BS = new Base_Station[N_OPT_BS];
		int cout_BS = 0;

		cq = new ConcurrentLinkedQueue();
		bs_q = new ConcurrentLinkedQueue[N_NODES];

		for (int i = 0; i < N_NODES; i++) { // if Optimal BS exists, create queue for it
			// 1. create queue for every optimal base station and create base station
			if (SL_data.getOptBS(i) != null) {
				this.bs_q[i] = new ConcurrentLinkedQueue<Integer>();
				BS[cout_BS] = new Base_Station(SL_data.getOptBS(i).key, this.bs_q[i], travelTime,
						Math.round(SL_data.getOptBS(i).getAssignedAmbulances()), false, w, simulationTravelWaitingTime,
						ambulancesReturnTimes);
				cout_BS++;
			}

			// 2. upload gathering centers' average daily calls as produced calls, optimal
			// BS, travel time, simulation travel+waiting time, return to BS time
			if (SL_data.getOpt(i) != null) {
				producedCalls[i] = SL_data.getOpt(i).getAverageDailyCalls();
				optimalBS[i] = SL_data.getOpt(i).nearestBS();

				dist_node dn_H = SL_data.getDistMatrix(i, SL_data.getOpt(i).H);
				dist_node dn_BS = SL_data.getDistMatrix(i, SL_data.getOpt(i).nearestBS());
				travelTime[i] = (dn_H.p + dn_BS.p) * SL_data.getTravelUnitValue();
				simulationTravelWaitingTime[i] = new gathering_time(SL_data.getOpt(i).key,
						SL_data.getOpt(i).getAverageDailyCalls());
				if (!only_gatherings) {
					generateReturnTimeArray(i);
				}
			}

		}

		Call_Producer = new Call_Producer(producedCalls, cq, N_NODES, SL_data.getTotalDailyCalls(), true, w);
		Dispatcher = new Dispatcher(cq, bs_q, optimalBS, false, w);
	}

	// Start Simulation
	public void simulate() throws InterruptedException {

		System.out.println("\t_SIMULATION_");

		// start base stations
		for (int i = 0; i < N_OPT_BS; i++) {
			BS_Threads[i] = new Thread(BS[i]);
			BS_Threads[i].start();
		}

		// start dispatcher
		Dispatcher_Thread = new Thread(Dispatcher);
		Dispatcher_Thread.start();

		w.composeFrame(); // compose frame for threads' text areas visualization

		// start producer
		Producer_Thread = new Thread(Call_Producer);
		Producer_Thread.start();

		while (Producer_Thread.isAlive()) { // until Producer has calls to create
		}
		System.out.println("Producer thread is dead.");

		Dispatcher.stop();
		while (Dispatcher_Thread.isAlive()) { // until Dispatcher has calls to dispatch
		}
		System.out.println("Dispatcher thread is dead.");

		for (int i = 0; i < N_OPT_BS; i++) {
			int k = BS[i].getKey();
			BS[i].stop();
			while (BS_Threads[i].isAlive()) { // until Base Station has calls to answer and ambulances unavailable
			}
			System.out.println("Base Station " + k + " thread is dead.");
		}

		printTravelWaitingTimes();

		System.out.println("\n\t_SIMULATION ENDED_\n");
	}

	// Construct Return to Base Station Time Array:
	// for every Gathering Center associates the Return Time from the nearest
	// Hospital to the Optimal Base Station
	// this can be done only if Dijkstra is applied to all nodes and not only to
	// gathering center nodes.
	private void generateReturnTimeArray(int i) {
		int nearest_H = SL_data.getOpt(i).H;
		int nearest_BS = SL_data.getOpt(i).nearestBS();
		dist_node dn = SL_data.getDistMatrix(nearest_BS, nearest_H);
		ambulancesReturnTimes[i] = (dn.p) * SL_data.getTravelUnitValue();
		System.out.println("For Base Station " + nearest_BS + " that sends an ambulance to Gathering " + i
				+ " the Return time is: " + ambulancesReturnTimes[i]);
	}

	// PRINTS
	private void printTravelWaitingTimes() {
		System.out.println("\n _GATHERING CENTERS' CALLS TOTAL TRAVEL + WAITING TIMES_");
		for (gathering_time gt : simulationTravelWaitingTime) {
			if (gt != null) {
				gt.printTimesArray();
			}
		}
	}
}