package main_package;

// Class for Data Structures
import data_analysis.DataAnalysis;
import data_structures.adj_List;
import data_structures.adj_node;
import data_structures.baseStation_node;
import data_structures.dist_node;
import data_structures.gather_node;

public class SL_data {
	private final static String graph_name = "Sierra Leone";
	private final static String project_name = "Leone_emergency_medical_system";
	private final static String datafilename = "INPUT_FILE_SIERRA_LEONE_v2";
	private final static int ADJ_ROW_START = 6;
	private final static int travel_unit_value = 1; // unit value in minutes
	private final static int deadline = 120; // 120 minutes (2 hours)

	private static int N_NODES;
	private static int N_BS = 0;
	private static int N_H = 0;
	private static int N_G = 0;

	private static int N_AMBULANCES;
	private static int TOTAL_DAILY_CALLS = 0; // total average daily calls on Sierra Leone territory. It will be
												// increased during the loading of gathering centers data
	private static int TIME_SLOT_CALLS = 0; // average time slot calls

	private final static String src_path = "C:\\Users\\Giulia\\eclipse-workspace\\" + project_name + "\\src\\";
	private final static String path = "C:\\Users\\Giulia\\eclipse-workspace\\" + project_name + "\\src\\"
			+ datafilename + ".txt";
	private final static String dataAnalysis_folder = "C:\\Users\\Giulia\\eclipse-workspace\\" + project_name
			+ "\\src\\data_analysis";

	private static int OptBS_count = 0; // Number of Optimal BaseStations (BaseStations used)
	private static int N_ALERTS = 0; // Number of Travel Times that exceeds the deadline

	// Node type Array: to know nodes' type (array's index is the node's key)
	// 0 : hospital, 1 : base_station, 2 : gathering, 3 : generic
	private static byte[] node_types;

	// Node coordinates Matrix
	private static int[][] node_coordinates;

	// Node name Array
	private static String[] node_names;

	// Optimal H and BS for Gathering nodes Array
	private static gather_node[] opt;

	// Optimal Allocation of Base Stations
	private static baseStation_node[] optBS;

	// Adjacency List Array
	private static adj_List[] adj;

	// Distances Matrix
	private static dist_node[][] M_dist;

	// Edges Matrix
	private static String[][] edges;

	// Isolations Array
	private static boolean[] isolations;

	// constructor
	public SL_data() {
		node_types = new byte[N_NODES];
		node_coordinates = new int[2][N_NODES];
		node_names = new String[N_NODES];
		opt = new gather_node[N_NODES];
		optBS = new baseStation_node[N_NODES];
		adj = new adj_List[N_NODES];
		M_dist = new dist_node[N_NODES][N_NODES];
		edges = new String[N_NODES][N_NODES];
		isolations = new boolean[N_NODES];
	}

	// GET
	public static int getNodes() {
		return N_NODES;
	}

	public static String getGraphName() {
		return graph_name;
	}

	public static String getDataFilename() {
		return datafilename;
	}

	public static int getAdjRowStart() {
		return ADJ_ROW_START;
	}

	public static int getTravelUnitValue() {
		return travel_unit_value;
	}

	public static int getDeadline() {
		return deadline;
	}

	public static int getBS() {
		return N_BS;
	}

	public static int getH() {
		return N_H;
	}

	public static int getG() {
		return N_G;
	}

	public static int getAmbulances() {
		return N_AMBULANCES;
	}

	public static int getTotalDailyCalls() {
		return TOTAL_DAILY_CALLS;
	}

	public static int getTimeSlotCalls() {
		return TIME_SLOT_CALLS;
	}

	public static String getSrcPath() {
		return src_path;
	}

	public static String getPath() {
		return path;
	}

	public static String getDataAnalysisFolder() {
		return dataAnalysis_folder;
	}

	public static short getNode_type(int key) {
		return node_types[key];
	}

	public static int getNodeX(int key) {
		return node_coordinates[0][key];
	}

	public static int getNodeY(int key) {
		return node_coordinates[1][key];
	}

	public static String getNode_name(int key) {
		return node_names[key];
	}

	public static gather_node getOpt(int key) {
		return opt[key];
	}

	public static baseStation_node getOptBS(int key) {
		return optBS[key];
	}

	public static adj_List getAdjList(int key) {
		return adj[key];
	}

	public static dist_node getDistMatrix(int row_key, int col_key) {
		return M_dist[row_key][col_key];
	}

	public static String getEdge(int row_key, int col_key) {
		return edges[row_key][col_key];
	}

	public static int getOptBSCount() {
		return OptBS_count;
	}

	public static int getAlerts() {
		return N_ALERTS;
	}

	public static boolean getIsolation(int key) {
		return isolations[key];
	}

	// SET
	public static void setNodes(int nodes) {
		N_NODES = nodes;
	}

	public static void setAmbulances(int ambulances) {
		N_AMBULANCES = ambulances;
	}

	public static void setNode_type(int key, byte type) {
		node_types[key] = type;
	}

	public static void setNodeX(int key, int X) {
		node_coordinates[0][key] = X;
	}

	public static void setNodeY(int key, int Y) {
		node_coordinates[1][key] = Y;
	}

	public static void setNode_name(int key, String name) {
		node_names[key] = name;
	}

	public static void setOpt(int key) {
		opt[key] = new gather_node(key);
	}

	public static void setOptBS(int key) {
		if (SL_data.getOptBS(key) == null) {
			optBS[key] = new baseStation_node(key);
		}
	}

	public static void setOptBS_count(int c) {
		OptBS_count = c;
	}

	public static void setAdjList(int key) {
		adj[key] = new adj_List();
	}

	public static void setDistMatrix(int row_key, int col_key) {
		M_dist[row_key][col_key] = new dist_node(col_key);
	}

	public static void setEdge(int row_key, int col_key, String edge) {
		edges[row_key][col_key] = edge;
	}

	public static void setTimeSlotCalls(int c) {
		TIME_SLOT_CALLS = c;
	}

	public static void setIsolation(int key, boolean isolated) {
		isolations[key] = isolated;
	}

	// INCREASE
	public static void increaseBS() {
		N_BS++;
	}

	public static void increaseH() {
		N_H++;
	}

	public static void increaseG() {
		N_G++;
	}

	public static void increaseTotalDailyCalls(int n_calls) {
		TOTAL_DAILY_CALLS = TOTAL_DAILY_CALLS + n_calls;
	}

	public static void increaseTimeSlotCalls(int n_calls) {
		TIME_SLOT_CALLS = TIME_SLOT_CALLS + n_calls;
	}

	public static void increaseOptBS() {
		OptBS_count++;
	}

	public static void increaseALERTS() {
		N_ALERTS++;
	}

	// PRINT

	// Prints Distances Matrix
	public static void printDistMatrix() {
		System.out.println("\n DISTANCES MATRIX");
		for (int i = 0; i < N_NODES; i++) { // print columns node keys
			System.out.print("\t\t\t" + i);
		}
		System.out.println();
		for (int i = 0; i < N_NODES; i++) { // print distances matrix data
			System.out.print(i);
			for (int j = 0; j < N_NODES; j++) {
				dist_node dn = SL_data.getDistMatrix(i, j);
				System.out.print("\t" + dn.key + "" + "|p:" + dn.p + "|prev:" + dn.prev + "|f:" + dn.f);
			}
			System.out.println("\n");
		}
	}

	// Prints Adjacency Lists
	public static void printAdjLists() {
		System.out.println("\n ADJACENCY LISTS\n");
		for (int i = 0; i < N_NODES; i++) {
			adj_node p = SL_data.getAdjList(i).head;
			System.out.print(i + " ");
			while (!(p == null)) {
				System.out.print("-->	key: " + p.key + " | cost: " + p.cost);
				System.out.print("\t");
				p = p.next;
			}
			System.out.println();
		}
	}

	// Prints Optimal data (Hospital and BaseStations' Ranking) for Gatherings
	public static void printGatheringsOpt(boolean on_file, DataAnalysis DA, String filename) {
		System.out.println("\n _GATHERINGS_ \n");
		if (on_file)
			DA.appendSimulationData(filename, "\n _GATHERINGS_ ");
		for (int i = 0; i < N_NODES; i++) {
			if (SL_data.getOpt(i) != null) {
				if (!SL_data.getIsolation(i)) {
					System.out.println(SL_data.getOpt(i).key + ". Gathering: " + SL_data.getNode_name(i));
					System.out.println("Nearest Hospital: " + SL_data.getOpt(i).H + " "
							+ SL_data.getNode_name(SL_data.getOpt(i).H));
					System.out.print("Base Station Ranking: ");
					SL_data.getOpt(i).printBS();
					dist_node dn_H = SL_data.getDistMatrix(i, SL_data.getOpt(i).H);
					dist_node dn_BS = SL_data.getDistMatrix(i, SL_data.getOpt(i).nearestBS());
					int totTravelTime = dn_H.p + dn_BS.p;
					int unit_value = SL_data.getTravelUnitValue(); // unit value in minutes
					int deadline = SL_data.getDeadline(); // 120 minutes (2 hours)
					System.out.println("Total Travel Time in units: " + totTravelTime);
					System.out.println("Total Travel Time in minutes: " + totTravelTime * unit_value);
					if (on_file) {
						DA.appendSimulationData(filename, "\n");
						DA.appendSimulationData(filename,
								SL_data.getOpt(i).key + ". Gathering: " + SL_data.getNode_name(i));
						DA.appendSimulationData(filename, "Nearest Hospital: " + SL_data.getOpt(i).H + " "
								+ SL_data.getNode_name(SL_data.getOpt(i).H));
						DA.appendSimulationData(filename, "Nearest BaseStation: " + SL_data.getOpt(i).nearestBS());
						DA.appendSimulationData(filename, "Total Travel Time in units: " + totTravelTime);
						DA.appendSimulationData(filename,
								"Total Travel Time in minutes: " + totTravelTime * unit_value);
					}
					if (totTravelTime * unit_value > deadline) {
						SL_data.increaseALERTS();
						int overtime = totTravelTime * unit_value - deadline;
						System.out
								.println("ALERT! Total Travel Time exceeds the deadline of " + overtime + " minutes!");
						if (on_file)
							DA.appendSimulationData(filename,
									"ALERT! Total Travel Time exceeds the deadline of " + overtime + " minutes!");
					}
				} else {
					System.out.println(SL_data.getOpt(i).key + ". Gathering: " + SL_data.getNode_name(i));
					System.out.println("Isolated.");
					if (on_file) {
						DA.appendSimulationData(filename, "\n");
						DA.appendSimulationData(filename,
								SL_data.getOpt(i).key + ". Gathering: " + SL_data.getNode_name(i));
						DA.appendSimulationData(filename, "Isolated.");
					}
				}
				System.out.println();
			}
		}
	}

	// Prints Optimal Base Stations' Set
	public static void printBaseStationsOpt(boolean on_file, DataAnalysis DA, String filename) {
		System.out.println("\n OPTIMAL BASE STATIONS");
		if (on_file)
			DA.appendSimulationData(filename, "\n OPTIMAL BASE STATIONS");
		SL_data.setOptBS_count(0); // reset counter
		for (int i = 0; i < N_NODES; i++) {
			if (SL_data.getOptBS(i) != null) {
				SL_data.increaseOptBS();
				baseStation_node bs = SL_data.getOptBS(i);
				System.out.println(bs.key + "." + SL_data.getNode_name(bs.key) + "\t| Tot Average Calls: "
						+ bs.getTotAverageDailyCalls() + "\tPercentage: " + bs.getDailyCallsPerc() + "%");
				if (on_file)
					DA.appendSimulationData(filename,
							bs.key + "." + SL_data.getNode_name(bs.key) + "\t| Tot Average Calls: "
									+ bs.getTotAverageDailyCalls() + "\tPercentage: " + bs.getDailyCallsPerc() + "%");
			}
		}
		System.out.println("Number of Base Stations used: " + SL_data.getOptBSCount() + " out of " + SL_data.getBS());
		System.out.println();
	}

	// Prints Actual Priorities for a node
	public static void printCurrentPriorities(int key) {
		System.out.print("key:\t\t\t");
		for (int i = 0; i < N_NODES; i++) { // print columns node keys
			System.out.print(SL_data.getDistMatrix(key, i).key + "\t");
		}
		System.out.println();
		System.out.print("current priority:\t");
		for (int i = 0; i < N_NODES; i++) { // print priorities
			System.out.print(SL_data.getDistMatrix(key, i).p + "\t");
		}
		System.out.println();
	}

	// Prints Edges' Matrix
	public static void printEdges() {
		System.out.println("\n EDGES MATRIX");
		for (int i = 0; i < N_NODES; i++) { // print columns node keys
			System.out.print("\t" + i);
		}
		System.out.println();
		for (int i = 0; i < N_NODES; i++) { // print edges matrix
			System.out.print(i);
			for (int j = 0; j < N_NODES; j++) {
				System.out.print("\t" + edges[i][j]);
			}
			System.out.println("\n");
		}
	}

	// Prints Ambulances Distribution
	public static void printAmbulancesDistribution(boolean on_file, DataAnalysis DA, String filename) {
		System.out.println("\n AMBULANCES DISTRIBUTION");
		if (on_file)
			DA.appendSimulationData(filename, "\n AMBULANCES DISTRIBUTION");
		for (int i = 0; i < N_NODES; i++) {
			if (SL_data.getOptBS(i) != null) {
				baseStation_node bs = SL_data.getOptBS(i);
				System.out.println(bs.key + "." + SL_data.getNode_name(bs.key) + "\t| Tot Average Calls: "
						+ bs.getTotAverageDailyCalls() + "\tPercentage: " + bs.getDailyCallsPerc() + "%"
						+ "\tAssigned Ambulances: " + bs.getAssignedAmbulances() + " -> "
						+ Math.round(bs.getAssignedAmbulances()));
				if (on_file)
					DA.appendSimulationData(filename,
							bs.key + "." + SL_data.getNode_name(bs.key) + "\t| Tot Average Calls: "
									+ bs.getTotAverageDailyCalls() + "\tPercentage: " + bs.getDailyCallsPerc() + "%"
									+ "\tAssigned Ambulances: " + bs.getAssignedAmbulances() + " -> "
									+ Math.round(bs.getAssignedAmbulances()));
			}
		}
		System.out.println();
	}

	// POPULATE

	// Populate Adjacency List
	public static boolean populateAdjList(int k, String[] node_data_v) {
		SL_data.setAdjList(k);
		if (!(node_data_v.length <= ADJ_ROW_START)) {
			adj_node p = null; // pointer to nodes

			for (int i = ADJ_ROW_START; i < node_data_v.length; i++) {
				String[] key_cost = node_data_v[i].split(" ");
				int key = Integer.parseInt(key_cost[0]);
				int cost = Integer.parseInt(key_cost[1]);

				adj_node new_node = new adj_node(key, cost);

				if (i == ADJ_ROW_START) {
					SL_data.getAdjList(k).head = new_node;
					p = SL_data.getAdjList(k).head;
				} else {
					p.next = new_node;
					p = p.next;
				}
			}
			p.next = null;
			return false; // the node has adjacency list, it is not isolated
		} else {
			System.out.println("Isolated: " + k);
			return true; // the node has no adjacency, it is isolated
		}
	}

	// DISTRIBUTE

	// distribute ambulances over optimal Base Stations
	public static void distributeAmbulances() {
		for (int i = 0; i < N_NODES; i++) {
			if (SL_data.getOptBS(i) != null) {
				SL_data.getOptBS(i).assignAmbulances(SL_data.getAmbulances());
			}
		}
	}
}