package main_package;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

// Acquire File Data

public class acquireFileData {

	static int node_counter; // node counter
	static int kc; // key counter

	// constructor
	public acquireFileData() {
		node_counter = 0;
		kc = 0;
	}

	public static int getFileData() {
		int N_NODES;

		try {
			File adj_file = new File(SL_data.getPath());
			Scanner myReader = new Scanner(adj_file);

			int nodes = Integer.valueOf(myReader.nextLine()); // acquire number of nodes
			int ambulances = Integer.valueOf(myReader.nextLine()); // acquire number of ambulances
			SL_data.setNodes(nodes);
			SL_data.setAmbulances(ambulances);
			new SL_data(); // construct data structures with number of nodes

			N_NODES = SL_data.getNodes();

			kc = 0;

			while (myReader.hasNextLine()) {
				node_counter++;

				String node_data = myReader.nextLine();
				String[] node_data_v = node_data.split(",");

				// Acquire NODE COORDINATES
				SL_data.setNodeX(kc, Integer.valueOf(node_data_v[1]));
				SL_data.setNodeY(kc, Integer.valueOf(node_data_v[2]));

				// Acquire NODE TYPE
				SL_data.setNode_type(kc, Byte.parseByte(node_data_v[3]));
				switch (SL_data.getNode_type(kc)) {
				case 0:
					SL_data.increaseH();
					break;
				case 1:
					SL_data.increaseBS();
					break;
				case 2:
					SL_data.increaseG();
					SL_data.setOpt(kc); // Prepare Optimal data structures for Gathering
					SL_data.getOpt(kc).setAverageDailyCalls(Integer.parseInt(node_data_v[4])); // Set average number of
																								// daily calls for
																								// gathering
					SL_data.increaseTotalDailyCalls(SL_data.getOpt(kc).getAverageDailyCalls());
					break;
				default:
					break;
				}

				// Acquire NODE NAME
				SL_data.setNode_name(kc, node_data_v[5]);

				// Prepare Distance Node with KEY
				for (int i = 0; i < N_NODES; i++) {
					SL_data.setDistMatrix(i, kc);
				}

				// Acquire Adjacency List
				boolean isolated_node = SL_data.populateAdjList(kc, node_data_v);
				SL_data.setIsolation(kc, isolated_node); // if node has no Adjacency List it is isolated

				kc++;

			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred during file data acquisition.");
			e.printStackTrace();
		}
		return node_counter;
	}

	// Get average calls of new time slot for gatherings centers from file
	public static int getFileDataTimeSlot(int timeslot) {
		String path = SL_data.getSrcPath() + SL_data.getDataFilename() + "_ts" + timeslot + ".txt";
		System.out.println(path);
		try {
			File ts_calls_file = new File(path);
			Scanner myReader = new Scanner(ts_calls_file);

			kc = 0;
			node_counter = 0;

			while (myReader.hasNextLine()) {
				node_counter++;
				String node_data = myReader.nextLine();
				String[] node_data_v = node_data.split(",");

				if (Byte.parseByte(node_data_v[1]) == 2) { // Gathering center type node
					SL_data.getOpt(kc).setAverageTimeSlotCalls(Byte.parseByte(node_data_v[2])); // Set average number of
																								// time slot calls for
																								// gathering
					SL_data.increaseTimeSlotCalls(SL_data.getOpt(kc).getAverageDailyCalls());
				}
				kc++;
			}
			myReader.close();
		} catch (FileNotFoundException e) {
			System.out.println("An error occurred during file data acquisition.");
			e.printStackTrace();
		}
		return node_counter;
	}
}