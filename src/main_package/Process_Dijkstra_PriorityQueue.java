package main_package;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

import data_structures.adj_node;
import data_structures.dist_node;

// Process Data with Dijkstra Algorithm using a Heap Priority Queue

public class Process_Dijkstra_PriorityQueue {

	private PriorityQueue<Integer> pq; // An unbounded priority queue based on a priority heap
	final static int N_NODES = SL_data.getNodes();
	private static int key; // key of node over which Dijkstra is applied
	private boolean optimized; // to optimize execution time and get only optimal configuration
	private boolean prints; // use System.out.print or not

	class The_Comparator implements Comparator<Integer> { // Used by PQ to compare queue's elements

		private int ref_key_node;

		public The_Comparator(int ref_key_node) {
			this.ref_key_node = ref_key_node;
		}

		public int compare(Integer key1, Integer key2) {
			Integer k1_priority = SL_data.getDistMatrix(ref_key_node, key1).p;
			Integer k2_priority = SL_data.getDistMatrix(ref_key_node, key2).p;

			if (k2_priority < k1_priority)
				return 1;
			return -1;
		}
	}

	// constructor
	public Process_Dijkstra_PriorityQueue(int k, boolean optimized, boolean prints) {
		key = k;
		this.pq = new PriorityQueue<Integer>(N_NODES, new The_Comparator(key));
		this.optimized = optimized;
		this.prints = prints;
	}

	// Process Dijkstra for single Node
	public void elab() {
		System.out.println("_________________________________________________");
		System.out.println("\nAPPLYING DIJKSTRA ON NODE " + key + "\n");

		enqueue();

		if (SL_data.getNode_type(key) == 2) { // check if gathering...
			if (prints)
				System.out.println("type: " + SL_data.getNode_type(key)
						+ ". The node on which Dijkstra is applied is a GATHERING.");
			SL_data.getOpt(key).createBS(SL_data.getBS());
		}

		while (!pq.isEmpty()) { // while queue is not empty
			if (prints)
				System.out.println("QUEUE: " + Arrays.toString(pq.toArray()));
			SL_data.printCurrentPriorities(key);

			int k = dequeue(); // extract minimum cost node from queue

			if (!SL_data.getDistMatrix(key, k).f) { // if extracted node k is not already fixed...
				if (prints)
					System.out.println("\t_Node " + k + " not fixed...");

				SL_data.getDistMatrix(key, k).f = true; // fix node k

				// Data for gatherings: get Optimal Data (Hospital and Base Station Ranking)
				if (SL_data.getNode_type(key) == 2) { // gathering type
					if (SL_data.getNode_type(k) == 0 && !SL_data.getOpt(key).fixedH()) {
						SL_data.getOpt(key).addH(k);
						System.out.println("We found Hospital with key: " + k);
					} else if (SL_data.getNode_type(k) == 1 && !SL_data.getOpt(key).fullBS(SL_data.getBS())) {
						if (SL_data.getOpt(key).getBSindex() == 0) {
							SL_data.setOptBS(k);
							SL_data.getOptBS(k).appendG(key);
							SL_data.getOptBS(k).addDailyCalls(SL_data.getOpt(key).getAverageDailyCalls(),
									SL_data.getTotalDailyCalls());
						}
						SL_data.getOpt(key).addBS(k, SL_data.getBS());
						System.out.println("We found BaseStation with key: " + k);
						SL_data.getOpt(key).printBS();
					}

					if (optimized) { // to optimize computation time, stop when the nearest H and nearest BS are
										// found
						if (SL_data.getOpt(key).fixedH() && SL_data.getOpt(key).getBSindex() > 0) {
							pq.clear(); // clear priority queue
							break; // exit Dijkstra's Algorithm application on this node
						}
					}
				}

				// get node adjacency list of extracted node k
				adj_node p = SL_data.getAdjList(k).head;
				while (p != null) { // walk on adjacency list of extracted node k
					dist_node dn = SL_data.getDistMatrix(key, p.key); // Adjacent node distance data
					if (prints)
						System.out.println("Adjacent Node:\t\t" + p.key + "\t|cost: " + p.cost + "\t|p: " + dn.p
								+ "\t|prev: " + dn.prev);
					if (!dn.f) { // if adjacent node not already fixed...
						update(p, dn, k); // update adjacent node distance data
					}
					p = p.next;
				}
			} else {
				if (prints)
					System.out.println("\t_Node " + k + " already fixed.");
			}
		}
		System.out.println("\n_EMPTY QUEUE. DIJKSTRA COMPLETED for node " + key);
	}

	// Enqueue
	private void enqueue() {
		// Source node
		// previous node kept on -1
		SL_data.getDistMatrix(key, key).p = 0; // priority
		pq.add(key); // enqueued
	}

	// Dequeue
	private int dequeue() {
		int k = pq.poll(); // extracted node key
		if (prints)
			System.out.println("\n- Extracted node: " + k);
		return k;
	}

	// Update
	private void update(adj_node p, dist_node dn, int k) {

		int tot_p = p.cost + SL_data.getDistMatrix(key, k).p; // total cost from node k

		if (tot_p < dn.p || dn.p == -1) {
			dn.p = tot_p;
			dn.prev = k;
			pq.add(p.key);

			if (prints)
				System.out.println("Adjacent Node update:\t" + p.key + "\t|cost: " + p.cost + "\t|p: " + dn.p
						+ "\t|prev: " + dn.prev);
		} else {
			if (prints)
				System.out.println("Adjacent Node unchan:\t" + p.key + "\t|cost: " + p.cost + "\t|p: " + dn.p
						+ "\t|prev: " + dn.prev);
		}
	}

}