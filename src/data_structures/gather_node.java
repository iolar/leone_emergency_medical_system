package data_structures;

public class gather_node {
	public int key; // Gathering node key
	public int H; // nearest Hospital key
	int[] BS; // nearest Base Station keys ranking

	int BS_index; // keep index of filled ranking

	int average_daily_calls; // average number of daily calls for this gathering
	int average_timeslot_calls; // average number of time slot calls for this gathering

	// constructor
	public gather_node(int key) {
		this.key = key;
		this.H = -1;
	}

	// GET
	public int getAverageDailyCalls() {
		return average_daily_calls;
	}

	public int getAverageTimeSlotCalls() {
		return average_timeslot_calls;
	}

	public int getBSindex() {
		return BS_index;
	}

	// SET
	public void setAverageDailyCalls(int adc) {
		average_daily_calls = adc;
	}

	public void setAverageTimeSlotCalls(int atsc) {
		average_timeslot_calls = atsc;
	}

	//
	public void createBS(int N_BS) {
		this.BS = new int[N_BS];
	}

	public boolean fullBS(int N_BS) {
		return BS_index >= N_BS;
	}

	public void addBS(int BS_key, int N_BS) {
		if (BS_index < N_BS) {
			BS[BS_index] = BS_key;
			BS_index++;
		} else
			System.out.println("Gathering BaseStation ranking full !");
	}

	public void printBS() {
		System.out.print("\t_Base Station ranking for node " + this.key + " :\t[");
		for (int BSnode : BS) {
			System.out.print(BSnode + " ");
		}
		System.out.println("]");
	}

	public int nearestBS() {
		return BS[0];
	}

	public boolean fixedH() {
		if (H != -1)
			return true;
		return false;
	}

	public void addH(int H_key) {
		if (H == -1) {
			H = H_key;
		} else
			System.out.println("Gathering Hospital fixed yet !");
	}
}