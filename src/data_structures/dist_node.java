package data_structures;

public class dist_node {
	public int key; // node key, indexes through code's structures
	public int p; // priority into the queue
	public int prev; // key of previous node in shortest path
	public boolean f; // node fixed for shortest path (true/false)

	// constructor
	public dist_node(int key) {
		this.key = key;
		this.p = -1;
		this.prev = -1;
		this.f = false;
	}
}