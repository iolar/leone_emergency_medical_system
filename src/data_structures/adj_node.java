package data_structures;

public class adj_node {
	public int key; // adjacent node key
	public int cost; // edge cost
	public adj_node next;

	// constructor
	public adj_node(int key, int cost) {
		this.key = key;
		this.cost = cost;
	}
}