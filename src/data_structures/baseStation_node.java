package data_structures;

import java.util.ArrayList;

public class baseStation_node {
	public int key; // Base Station node key
	int tot_average_daily_calls; // Total average daily calls of assigned gathering centers
	float daily_calls_percentage;// Daily calls percentage respect to total daily calls
	float assigned_ambulances;
	ArrayList<Integer> G; // Assigned gathering centers' keys list

	// GET
	public int getTotAverageDailyCalls() {
		return tot_average_daily_calls;
	}

	public float getDailyCallsPerc() {
		return daily_calls_percentage;
	}

	public float getAssignedAmbulances() {
		return assigned_ambulances;
	}

	public baseStation_node(int key) {
		this.key = key;
		this.tot_average_daily_calls = 0;
		this.daily_calls_percentage = 0.0f;
		this.assigned_ambulances = 0;
		this.G = new ArrayList<Integer>();
	}

	public void addDailyCalls(int n_daily_calls, int tot_daily_calls) { // add average daily calls from an assigned
																		// gathering center to the total
		this.tot_average_daily_calls = this.tot_average_daily_calls + n_daily_calls;
		this.daily_calls_percentage = this.daily_calls_percentage + ((float) n_daily_calls * 100 / tot_daily_calls);
	}

	public void appendG(int key) {
		this.G.add(key); // Assign gathering center to base station
	}

	public void assignAmbulances(int tot_ambulances) {
		this.assigned_ambulances = tot_ambulances * this.daily_calls_percentage / 100;
	}

	// TIME SLOT
	public void newTimeSlot() { // prepare variables for a new time slot
		this.tot_average_daily_calls = 0;
		this.daily_calls_percentage = 0.0f;
		this.assigned_ambulances = 0;
	}

	public void fillTimeSlot(int ts_calls, int tot_daily_calls) {
		this.tot_average_daily_calls = this.tot_average_daily_calls + ts_calls;
		this.daily_calls_percentage = this.daily_calls_percentage + ((float) ts_calls * 100 / tot_daily_calls);
	}
}