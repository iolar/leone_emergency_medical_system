package dynamic_simulation;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JTextArea;

import java.awt.Color;

public class Base_Station implements Runnable {

	private static boolean exit;
	private Integer key; // Base Station node key
	private ConcurrentLinkedQueue<Integer> available_ambulances; // queue of available ambulances for this BaseStation
	private ConcurrentLinkedQueue<Integer> q; // queue of incoming calls for base station
	private PriorityQueue<Integer> pq; // priority queue to know waiting time for next available ambulance

	private Integer N_AMB; // number of ambulances assigned to this base station

	private Ambulance[] A; // array of ambulances assigned to this base station
	private Thread[] Ambulances_Thread;
	private Integer[] travelTime; // Travel Times for every Gathering Center to be assisted from its own optimal
									// BaseStation to Optimal Hospital
	private gathering_time[] simulationTravelWaitingTime; // Travel Time + Waiting Time in simulation for every
															// Gathering Center to be assisted from its own optimal
															// BaseStation to Optimal Hospital
	private Integer[] ambulancesReturnTimes; // Return Times for every gathering center from the nearest hospital to the
												// optimal base station

	private int assist_freq = 1; // interval in seconds to check if some calls are in queue
	private boolean slow; // slow simulation
	private String t = "\n _Base Station ";

	private int wait_for_ambulance = 5; // if no ambulance is available, this is wait for ambulance time

	private Window w;
	private JTextArea ta; // text area on which write

	Color green = new Color(193, 255, 158);
	Color red = new Color(255, 191, 191);

	class The_Comparator implements Comparator<Integer> { // Used by PQ to compare queue's elements

		public The_Comparator(int amb_id) {
		}

		public int compare(Integer id1, Integer id2) {
			Integer id1_priority = A[id1].getWaitingTime();
			Integer id2_priority = A[id2].getWaitingTime();

			if (id2_priority < id1_priority)
				return 1;
			return -1;
		}
	}

	// constructor
	public Base_Station(Integer key, ConcurrentLinkedQueue<Integer> q, Integer[] travelTime, Integer N_AMB,
			boolean slow, Window w, gathering_time[] simulationTravelWaitingTime, Integer[] ambulancesReturnTimes) {
		this.w = w;
		exit = false;
		this.key = key;
		available_ambulances = new ConcurrentLinkedQueue<Integer>();
		this.pq = new PriorityQueue<Integer>(N_AMB, new The_Comparator(key));
		this.q = q;
		this.N_AMB = N_AMB;
		this.A = new Ambulance[N_AMB]; // construct ambulances' array

		ta = w.panel(t, "Base Station", String.valueOf(key), "INTO BASE STATION " + key + " THREAD", 8, 40, green); // create
																													// panel
																													// for
																													// frame,
																													// return
																													// text
																													// area

		for (int i = 0; i < N_AMB; i++) {
			A[i] = new Ambulance(available_ambulances, key, ta, w); // construct ambulances
			available_ambulances.add(i); // place available ambulance
			Ambulances_Thread = new Thread[N_AMB]; // construct ambulances' threads array
		}
		this.travelTime = travelTime;
		this.simulationTravelWaitingTime = simulationTravelWaitingTime;
		this.ambulancesReturnTimes = ambulancesReturnTimes;
		this.slow = slow;
	}

	public void run() {
		while (!exit) {
			if (slow) {
				try {
					TimeUnit.SECONDS.sleep(assist_freq);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			manageCalls();
		}

		// conclusion
		System.out.println(t + key + " finishing emptying calls' queue and waiting for ambulances");
		w.print_on_text_area(ta, t + key + " finishing emptying calls' queue and waiting for ambulances");
		if (q.isEmpty()) {
			System.out.println(t + key + ": EMPTY QUEUE");
			w.print_on_text_area(ta, t + key + ": EMPTY QUEUE");
		} else {
			manageCalls();
		}
		while (available_ambulances.size() != N_AMB) {// waits for ambulances to complete assist
		}
		System.out.println(t + key + " ALL AMBULANCES HAVE COMPLETED TRAVEL");
		w.print_on_text_area(ta, t + key + " ALL AMBULANCES HAVE COMPLETED TRAVEL");
		///
	}

	public void stop() {
		exit = true;
	}

	public int getKey() {
		return key;
	}

	private void manageCalls() {
		while (!q.isEmpty()) { // queue with calls
			int g; // for extraction of call
			if (!available_ambulances.isEmpty()) { // ambulance available
				w.setTextAreaBackgroudColor(ta, green);
				int amb_size = available_ambulances.size();

				System.out.println(t + key + ": " + " Available ambulances: " + amb_size);
				w.print_on_text_area(ta, t + key + ": " + " Available ambulances: " + amb_size);

				int a = available_ambulances.poll(); // book ambulance
				g = q.poll(); // extract call from queue
				simulationTravelWaitingTime[g].addTime(travelTime[g]); // append travel time for gathering center's call
				A[a].setG(g); // set gathering center's key
				A[a].setId(a); // set ambulance id
				if (ambulancesReturnTimes[g] != null) {
					A[a].setTime(travelTime[g] + ambulancesReturnTimes[g]); // set travel time from BaseStation to
																			// GatheringCenter and from GatheringCenter
																			// to Hospital and from Hospital to Base
																			// Station
				} else {
					A[a].setTime(travelTime[g]); // set travel time from BaseStation to GatheringCenter and from
													// GatheringCenter to Hospital
				}
				pq.add(a);

				System.out.println(t + key + ": " + "has sent ambulance " + a + " to assist gathering center " + g);
				w.print_on_text_area(ta,
						t + key + ": " + "has sent ambulance " + a + " to assist gathering center " + g);

				Ambulances_Thread[a] = new Thread(A[a]); // construct ambulance thread
				Ambulances_Thread[a].start();
			} else {
				w.setTextAreaBackgroudColor(ta, red);
				int waiting_time = A[pq.peek()].getWaitingTime();

				System.out.println(
						t + key + ": " + "no ambulance to assist gathering center. Next available ambulance in: "
								+ waiting_time + " min.\n");
				w.print_on_text_area(ta,
						t + key + ": " + "no ambulance to assist gathering center.\nNext available ambulance in: "
								+ waiting_time + " min.\n");

				g = q.peek(); // read call's gathering center's key, not extract
				simulationTravelWaitingTime[g].addWaiting(waiting_time); // add to matrix the time to start serve
																			// gathering center's call (waiting time)

				try {
					TimeUnit.SECONDS.sleep(wait_for_ambulance);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}