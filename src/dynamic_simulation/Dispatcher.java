package dynamic_simulation;

import java.awt.Color;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JTextArea;

public class Dispatcher implements Runnable {

	private static boolean exit;
	private ConcurrentLinkedQueue<Integer> cq;
	private ConcurrentLinkedQueue<Integer>[] bs_q;
	private Integer[] optimalBS;
	private int check_freq = 1; // interval in seconds to check if some calls are in queue
	private int call_G_key; // extracted from queue the key of the gathering center that sent a call
	private String t = "\n_Dispatcher: ";
	private boolean slow; // slow simulation

	private Window w;
	private JTextArea ta;
	private Color color;

	// constructor
	public Dispatcher(ConcurrentLinkedQueue<Integer> cq, ConcurrentLinkedQueue<Integer>[] bs_q, Integer[] optimalBS,
			boolean slow, Window w) {
		this.w = w;
		exit = false;
		this.cq = cq;
		this.bs_q = bs_q;
		this.optimalBS = optimalBS;
		this.slow = slow;

		color = new Color(227, 227, 84);
		ta = w.panel(t, "Dispatcher", "", "INTO DISPATCHER THREAD", 10, 40, color);
	}

	public void run() {
		while (!exit) {
			if (slow) {
				try {
					TimeUnit.SECONDS.sleep(check_freq);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			manageCalls();
		}

		// conclusion
		System.out.println(t + "finishing emptying calls' queue");
		w.print_on_text_area(ta, t + "finishing emptying calls' queue");
		if (cq.isEmpty()) {
			System.out.println(t + "Empty queue");
			w.print_on_text_area(ta, t + "Empty queue");
		} else {
			manageCalls();
		}
		///
	}

	public void stop() {
		exit = true;
	}

	private void manageCalls() {
		while (!cq.isEmpty()) {
			call_G_key = cq.poll();
			int bs = optimalBS[call_G_key]; // key of optimal BS for call's gathering center
			bs_q[bs].add(call_G_key); // add to optimal BS queue
			printBSqueue(bs);
		}
	}

	private void printBSqueue(int i) {
		if (bs_q[i] != null) {
			if (!bs_q[i].isEmpty()) {
				String bs_queue = t + "BS queue " + i + " " + Arrays.toString(bs_q[i].toArray());
				System.out.println(bs_queue);
				w.print_on_text_area(ta, bs_queue);
			}
		}
	}
}