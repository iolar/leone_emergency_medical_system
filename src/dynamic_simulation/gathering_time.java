package dynamic_simulation;

public class gathering_time {
	private int key; // key of gathering center
	private int[] simTravelWaitTime; // Total simulation Travel + Waiting Time array
	private int count;

	// constructor
	public gathering_time(int key, int average_daily_calls) {
		this.key = key;
		this.simTravelWaitTime = new int[average_daily_calls];
		count = 0;
	}

	public void addTime(int travel_time) { // append new Travel time for gathering center's call
		simTravelWaitTime[count] += travel_time;
		count++;
	}

	public void addWaiting(int waiting_time) {
		if (simTravelWaitTime[count] == 0) {
			simTravelWaitTime[count] += waiting_time;
		}
	}

	public void printTimesArray() {
		System.out.print("Gathering " + key + " calls' travel + waiting times: ");
		for (int time : simTravelWaitTime) {
			System.out.print(time + " ");
		}
		System.out.print(";\n");
	}

}