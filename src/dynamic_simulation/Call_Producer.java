package dynamic_simulation;

import java.awt.Color;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.JTextArea;

public class Call_Producer implements Runnable {

	private Integer[] producedCalls;
	private ConcurrentLinkedQueue<Integer> cq;
	private int N_NODES;
	private int TOTAL_DAILY_CALLS;
	private int gen_freq = 3; // seconds to generate a new call from a gathering center
	private Random rand;
	private int rand_n;
	private String t = "\n_Producer: ";
	private boolean slow; // slow simulation

	private Window w;
	private JTextArea ta; // text area on which write
	private Color color;
	private JLabel remaining_calls_label; // label for visualize remaining calls during dynamic simulation

	// constructor
	public Call_Producer(Integer[] producedCalls, ConcurrentLinkedQueue<Integer> cq, int N_NODES, int totcalls,
			boolean slow, Window w) {
		this.w = w;
		this.producedCalls = producedCalls;
		this.cq = cq;
		this.N_NODES = N_NODES;
		this.TOTAL_DAILY_CALLS = totcalls;
		this.rand = new Random();
		this.slow = slow;

		color = new Color(227, 186, 84);
		remaining_calls_label = w.calls_panel(String.valueOf(TOTAL_DAILY_CALLS));
		ta = w.panel(t, "Producer", "", "INTO PRODUCER THREAD", 10, 40, color); // create panel for frame, return text
																				// area
	}

	public void run() {

		System.out.println(t + "INTO THE PRODUCER THREAD");

		while (TOTAL_DAILY_CALLS > 0) {

			if (slow) {
				try {
					TimeUnit.SECONDS.sleep(gen_freq);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			do {
				rand_n = rand.nextInt(N_NODES);
			} while (producedCalls[rand_n] == null || producedCalls[rand_n] == 0);

			System.out.println(t + "call from Gathering " + rand_n + " added to queue to be dispatched.");
			w.print_on_text_area(ta, t + "call from Gathering " + rand_n + " added to queue to be dispatched.");

			cq.add(rand_n);

			producedCalls[rand_n]--;
			TOTAL_DAILY_CALLS--;
			w.print_remaining_calls_label(remaining_calls_label, String.valueOf(TOTAL_DAILY_CALLS));
		}

		System.out.println(t + "GENERATED ALL DAILY CALLS.");
		w.print_on_text_area(ta, t + "GENERATED ALL DAILY CALLS.");

	}
}