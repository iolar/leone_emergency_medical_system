package dynamic_simulation;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JTextArea;

public class Ambulance implements Runnable {

	private Integer G; // Key of the gathering center to assist
	private Integer id; // ambulance id
	private Integer BS; // associated BaseStation
	private ConcurrentLinkedQueue<Integer> parking; // ambulance parking for availability into base station
	private String t = "\n _Ambulance ";
	private int time;
	private int waiting_time;

	private Window w;
	private JTextArea ta;

	// constructor
	public Ambulance(ConcurrentLinkedQueue<Integer> available_ambulances_queue, Integer BS, JTextArea ta, Window w) {
		this.w = w;
		this.parking = available_ambulances_queue;
		this.BS = BS;
		this.ta = ta;
	}

	public void run() {

		System.out.println(
				t + id + " of BS " + BS + " moving for gathering center " + G + ". Travel time: " + time + " min");
		w.print_on_text_area(ta,
				t + id + " of BS " + BS + " moving for gathering center " + G + ". Travel time: " + time + " min");

		for (int i = 1; i < time; i++) {
			try {
				TimeUnit.SECONDS.sleep(1);
				waiting_time--;
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("\n" + t + id + " of BS " + BS + " has assisted gathering " + G + "\n");
		w.print_on_text_area(ta, "\n" + t + id + " of BS " + BS + " has assisted gathering " + G + "\n");
		parking.add(id);
	}

	public void setG(Integer G) {
		this.G = G;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setTime(int time) {
		this.time = time;
		this.waiting_time = time;
	}

	public int getWaitingTime() {
		return waiting_time;
	}
}