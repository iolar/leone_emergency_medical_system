package dynamic_simulation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Window {

	private JFrame f;
	private JPanel p;
	private JPanel p_principal;
	private JPanel p_prod_disp;
	private JPanel p_bs;

	// constructor
	public Window(int N_OPT_BS) {
		f = new JFrame("Dynamic Simulation");
		f.setLayout(new GridLayout());
		f.setSize(1000, 750);
		p = new JPanel(new FlowLayout());
		p_principal = new JPanel(new BorderLayout());
		p_prod_disp = new JPanel(new GridLayout(2, 1));
		p_bs = new JPanel(new GridLayout(N_OPT_BS, 1));
	}

	// Create label to visualize remaining daily calls that Producer thread has to
	// generate
	public synchronized JLabel calls_panel(String totcalls) {
		JLabel remaining_calls_label = new JLabel(totcalls);
		JLabel title_label = new JLabel("Remaining Daily Calls: ");
		remaining_calls_label.setFont(new Font(remaining_calls_label.getFont().getName(), Font.BOLD, 16));
		title_label.setFont(new Font(title_label.getFont().getName(), Font.BOLD, 16));
		JPanel panel = new JPanel(new FlowLayout());
		JPanel panel_bl = new JPanel(new BorderLayout());

		panel.add(title_label);
		panel.add(remaining_calls_label);
		panel_bl.add(panel, BorderLayout.SOUTH);
		p_principal.add(panel_bl, BorderLayout.NORTH);

		return remaining_calls_label;
	}

	// Create JPanel for thread
	public synchronized JTextArea panel(String t, String label, String key, String textarea, int h, int w,
			Color color) {

		JPanel p_thread = new JPanel(new FlowLayout());

		JPanel p_l = new JPanel();
		JLabel l = new JLabel(label + " " + key);
		p_l.add(l);

		JPanel p_ta = new JPanel();

		JTextArea ta = new JTextArea(t + textarea, h, w);

		ta.setBackground(color);
		JScrollPane scrollPane = new JScrollPane(ta);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				e.getAdjustable().setValue(e.getAdjustable().getMaximum());
			}
		});
		p_ta.add(scrollPane);

		p_thread.add(p_l);
		p_thread.add(p_ta);

		switch (label) {
		case "Producer":
			p_prod_disp.add(p_thread, BorderLayout.CENTER);
			p_principal.add(p_prod_disp);
			break;
		case "Dispatcher":
			p_prod_disp.add(p_thread, BorderLayout.CENTER);
			p_principal.add(p_prod_disp);
			break;
		case "Base Station":
			p_bs.add(p_thread);
			break;
		default:
			break;
		}

		return ta;
	}

	// Compose Frame with created panels
	public synchronized void composeFrame() {
		p.add(p_principal);
		p.add(p_bs);
		p.setVisible(true);
		f.add(p);
		f.pack();
		f.setVisible(true);
	}

	// print on a text area of the frame
	public synchronized void print_on_text_area(JTextArea ta, String s) {
		ta.append(s);
	}

	// set background color of a text area of the frame
	public synchronized void setTextAreaBackgroudColor(JTextArea ta, Color color) {
		ta.setBackground(color);
	}

	// set remaining calls label to updated number of remaining daily calls to be
	// produced by the Producer thread
	public synchronized void print_remaining_calls_label(JLabel l, String remaining_calls) {
		l.setText(remaining_calls);
	}
}