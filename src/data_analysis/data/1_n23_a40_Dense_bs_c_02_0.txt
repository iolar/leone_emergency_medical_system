
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 12
Total Travel Time in units: 10
Total Travel Time in minutes: 50


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 12
Total Travel Time in units: 30
Total Travel Time in minutes: 150
ALERT! Total Travel Time exceeds the deadline of 30 minutes!


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 29
Total Travel Time in minutes: 145
ALERT! Total Travel Time exceeds the deadline of 25 minutes!


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 12
Total Travel Time in units: 10
Total Travel Time in minutes: 50


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 12
Total Travel Time in units: 24
Total Travel Time in minutes: 120


22. Gathering: Bure
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 26
Total Travel Time in minutes: 130
ALERT! Total Travel Time exceeds the deadline of 10 minutes!

 OPTIMAL BASE STATIONS
12.Yengema	| Tot Average Daily Calls: 55	Percentage: 88.70968%
14.Kamera	| Tot Average Daily Calls: 7	Percentage: 11.290323%

 AMBULANCES DISTRIBUTION
12.Yengema	| Tot Average Daily Calls: 55	Percentage: 88.70968%	Assigned Ambulances: 35.48387 -> 35
14.Kamera	| Tot Average Daily Calls: 7	Percentage: 11.290323%	Assigned Ambulances: 4.516129 -> 5
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 2
Gathering Centers: 8
Number of Base Stations used: 2 out of 2
Alerts: 3 over 8 gathering centers
Ambulances: 40
Total Average Daily Calls: 62
Ambulances over Daily Calls: Lack of 22
