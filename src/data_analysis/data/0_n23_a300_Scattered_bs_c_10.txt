
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 5
Total Travel Time in units: 6
Total Travel Time in minutes: 30


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 7
Total Travel Time in units: 16
Total Travel Time in minutes: 80


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 15
Total Travel Time in units: 15
Total Travel Time in minutes: 75


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 0
Total Travel Time in minutes: 0


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 23
Total Travel Time in minutes: 115

 OPTIMAL BASE STATIONS
5.	Simria	| Tot Average Daily Calls: 5	Percentage: 8.064516%
7.	Kulanko	| Tot Average Daily Calls: 6	Percentage: 9.67742%
12.	Yengema	| Tot Average Daily Calls: 17	Percentage: 27.419353%
15.	Sinegal	| Tot Average Daily Calls: 4	Percentage: 6.451613%
18.	Makeni	| Tot Average Daily Calls: 12	Percentage: 19.35484%
21.	Suniya	| Tot Average Daily Calls: 18	Percentage: 29.032257%

 AMBULANCES DISTRIBUTION
5.	Simria	| Tot Average Daily Calls: 5	Percentage: 8.064516%	Assigned Ambulances: 24.193548 -> 24
7.	Kulanko	| Tot Average Daily Calls: 6	Percentage: 9.67742%	Assigned Ambulances: 29.032259 -> 29
12.	Yengema	| Tot Average Daily Calls: 17	Percentage: 27.419353%	Assigned Ambulances: 82.25806 -> 82
15.	Sinegal	| Tot Average Daily Calls: 4	Percentage: 6.451613%	Assigned Ambulances: 19.35484 -> 19
18.	Makeni	| Tot Average Daily Calls: 12	Percentage: 19.35484%	Assigned Ambulances: 58.064518 -> 58
21.	Suniya	| Tot Average Daily Calls: 18	Percentage: 29.032257%	Assigned Ambulances: 87.09677 -> 87
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 10
Gathering Centers: 8
Number of Base Stations used: 6 out of 10
Alerts: 0 over 8 gathering centers
Ambulances: 300
Total Average Daily Calls: 62
Ambulances over Daily Calls: Excess of 238
