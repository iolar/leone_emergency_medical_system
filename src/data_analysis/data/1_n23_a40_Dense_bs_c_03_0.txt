
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 12
Total Travel Time in units: 10
Total Travel Time in minutes: 50


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 12
Total Travel Time in units: 30
Total Travel Time in minutes: 150
ALERT! Total Travel Time exceeds the deadline of 30 minutes!


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 29
Total Travel Time in minutes: 145
ALERT! Total Travel Time exceeds the deadline of 25 minutes!


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 7
Total Travel Time in minutes: 35


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 13 Karina
Nearest BaseStation: 21
Total Travel Time in units: 19
Total Travel Time in minutes: 95

 OPTIMAL BASE STATIONS
12.Yengema	| Tot Average Daily Calls: 28	Percentage: 45.161293%
14.Kamera	| Tot Average Daily Calls: 4	Percentage: 6.451613%
21.Suniya	| Tot Average Daily Calls: 30	Percentage: 48.387096%

 AMBULANCES DISTRIBUTION
12.Yengema	| Tot Average Daily Calls: 28	Percentage: 45.161293%	Assigned Ambulances: 18.064516 -> 18
14.Kamera	| Tot Average Daily Calls: 4	Percentage: 6.451613%	Assigned Ambulances: 2.580645 -> 3
21.Suniya	| Tot Average Daily Calls: 30	Percentage: 48.387096%	Assigned Ambulances: 19.35484 -> 19
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 3
Gathering Centers: 8
Number of Base Stations used: 3 out of 3
Alerts: 2 over 8 gathering centers
Ambulances: 40
Total Average Daily Calls: 62
Ambulances over Daily Calls: Lack of 22
