
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 8
Total Travel Time in minutes: 40


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 7
Total Travel Time in units: 13
Total Travel Time in minutes: 65


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 14
Total Travel Time in units: 29
Total Travel Time in minutes: 145
ALERT! Total Travel Time exceeds the deadline of 25 minutes!


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 18
Total Travel Time in units: 0
Total Travel Time in minutes: 0


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 13 Karina
Nearest BaseStation: 21
Total Travel Time in units: 19
Total Travel Time in minutes: 95

 OPTIMAL BASE STATIONS
7.Kulanko	| Tot Average Daily Calls: 6	Percentage: 9.67742%
12.Yengema	| Tot Average Daily Calls: 17	Percentage: 27.419353%
14.Kamera	| Tot Average Daily Calls: 4	Percentage: 6.451613%
18.Makeni	| Tot Average Daily Calls: 17	Percentage: 27.419355%
21.Suniya	| Tot Average Daily Calls: 18	Percentage: 29.032257%

 AMBULANCES DISTRIBUTION
7.Kulanko	| Tot Average Daily Calls: 6	Percentage: 9.67742%	Assigned Ambulances: 3.870968 -> 4
12.Yengema	| Tot Average Daily Calls: 17	Percentage: 27.419353%	Assigned Ambulances: 10.967742 -> 11
14.Kamera	| Tot Average Daily Calls: 4	Percentage: 6.451613%	Assigned Ambulances: 2.580645 -> 3
18.Makeni	| Tot Average Daily Calls: 17	Percentage: 27.419355%	Assigned Ambulances: 10.967742 -> 11
21.Suniya	| Tot Average Daily Calls: 18	Percentage: 29.032257%	Assigned Ambulances: 11.612903 -> 12
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 6
Gathering Centers: 8
Number of Base Stations used: 5 out of 6
Alerts: 1 over 8 gathering centers
Ambulances: 40
Total Average Daily Calls: 62
Ambulances over Daily Calls: Lack of 22
