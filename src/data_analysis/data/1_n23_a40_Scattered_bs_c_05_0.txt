
 _GATHERINGS_ 


0. Gathering: Makende
Nearest Hospital: 17 Makeni
Nearest BaseStation: 1
Total Travel Time in units: 8
Total Travel Time in minutes: 40


4. Gathering: Bumbe
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 3
Total Travel Time in minutes: 15


10. Gathering: Firawa
Nearest Hospital: 6 Kania
Nearest BaseStation: 7
Total Travel Time in units: 16
Total Travel Time in minutes: 80


11. Gathering: Kawama
Nearest Hospital: 3 Masumbiri
Nearest BaseStation: 12
Total Travel Time in units: 13
Total Travel Time in minutes: 65


16. Gathering: Fadugu
Nearest Hospital: 13 Karina
Nearest BaseStation: 21
Total Travel Time in units: 32
Total Travel Time in minutes: 160
ALERT! Total Travel Time exceeds the deadline of 40 minutes!


19. Gathering: Makeni
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 7
Total Travel Time in minutes: 35


20. Gathering: Mahakoi
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 14
Total Travel Time in minutes: 70


22. Gathering: Bure
Nearest Hospital: 17 Makeni
Nearest BaseStation: 21
Total Travel Time in units: 23
Total Travel Time in minutes: 115

 OPTIMAL BASE STATIONS
1.Pete	| Tot Average Daily Calls: 5	Percentage: 8.064516%
7.Kulanko	| Tot Average Daily Calls: 6	Percentage: 9.67742%
12.Yengema	| Tot Average Daily Calls: 17	Percentage: 27.419353%
21.Suniya	| Tot Average Daily Calls: 34	Percentage: 54.83871%

 AMBULANCES DISTRIBUTION
1.Pete	| Tot Average Daily Calls: 5	Percentage: 8.064516%	Assigned Ambulances: 3.2258062 -> 3
7.Kulanko	| Tot Average Daily Calls: 6	Percentage: 9.67742%	Assigned Ambulances: 3.870968 -> 4
12.Yengema	| Tot Average Daily Calls: 17	Percentage: 27.419353%	Assigned Ambulances: 10.967742 -> 11
21.Suniya	| Tot Average Daily Calls: 34	Percentage: 54.83871%	Assigned Ambulances: 21.935484 -> 22
----------------------------------------
Nodes: 23
Hospitals: 4
Base Stations: 5
Gathering Centers: 8
Number of Base Stations used: 4 out of 5
Alerts: 1 over 8 gathering centers
Ambulances: 40
Total Average Daily Calls: 62
Ambulances over Daily Calls: Lack of 22
