package data_analysis;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class DataAnalysis {

	private String dataAnalysis_folder;
	private static final String data_folder_name = "data";
	private static final String exec_folder_name = "Execution Times";

	// constructor
	public DataAnalysis(String dataAnalysis_folder) {
		this.dataAnalysis_folder = dataAnalysis_folder;

		// Prepare Folders
		createFolder(dataAnalysis_folder + "\\" + data_folder_name);
		createFolder(dataAnalysis_folder + "\\" + exec_folder_name);
	}

	// Create folders for Data Analysis
	public int createFolder(String folder_name) {

		String dir = folder_name + "\\";
		System.out.println(dir);

		try {
			Path dir_path = Paths.get(dir);
			if (!Files.exists(dir_path)) {
				Files.createDirectory(dir_path);
				System.out.println("Directory is created!");
			} else {
				System.out.println("Folder already exists.");
			}

		} catch (IOException e) {
			System.err.println("Failed to create directory! " + e.getMessage());
			return -1;
		}

		return 0;
	}

	// Write file with average total algorithm time through executions
	public int writeFileExecTime(String filename, long totAlgoTime) {

		String file_path = dataAnalysis_folder + "\\" + exec_folder_name + "\\" + filename;
		File time_file = new File(file_path);

		if (!time_file.exists()) {
			try {
				time_file.createNewFile();
				Files.writeString(Paths.get(file_path), String.valueOf(totAlgoTime)); // algorithm first execution time
																						// caught
				Files.writeString(Paths.get(file_path), "\n1", StandardOpenOption.APPEND); // One execution
				Files.writeString(Paths.get(file_path), "\n" + String.valueOf(totAlgoTime), StandardOpenOption.APPEND); // average
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}
		} else {
			try {
				Scanner myReader = new Scanner(time_file);
				String time_sum = myReader.nextLine();
				String n_executions = myReader.nextLine();
				long totTime = totAlgoTime + Long.valueOf(time_sum);
				int n_exec = 1 + Integer.valueOf(n_executions);
				Files.writeString(Paths.get(file_path), String.valueOf(totTime)); // summation of algorithm execution
																					// times
				Files.writeString(Paths.get(file_path), "\n" + String.valueOf(n_exec), StandardOpenOption.APPEND); // number
																													// of
																													// algorithm
																													// executions
				Files.writeString(Paths.get(file_path), "\n" + String.valueOf(totTime / n_exec),
						StandardOpenOption.APPEND); // average
				myReader.close();
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}
		}
		return 0;
	}

	// Clean Simulation Data File
	public int cleanDataFile(String filename) {
		String file_path = dataAnalysis_folder + "\\" + data_folder_name + "\\" + filename;
		File sim_file = new File(file_path);
		if (!sim_file.exists()) {
			try {
				sim_file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}
		}
		try {
			Scanner myReader = new Scanner(sim_file);
			Files.writeString(Paths.get(file_path), "");
			myReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
		return 0;
	}

	// Append Simulation Data to File
	public int appendSimulationData(String filename, String data) {
		String file_path = dataAnalysis_folder + "\\" + data_folder_name + "\\" + filename;
		File sim_file = new File(file_path);
		if (!sim_file.exists()) {
			try {
				sim_file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}
		}
		try {
			Scanner myReader = new Scanner(sim_file);
			Files.writeString(Paths.get(file_path), data + "\n", StandardOpenOption.APPEND);
			myReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return -1;
		}
		return 0;
	}
}